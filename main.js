//конструкция try catch нужна для того, чтобы перехватывать ощибки и обрабатывать их. Мы можем ее использовать когда хотим проверить свой код на ошибки, 
//для проверки корректности воодимых данных от пользоваетля
//можно использовать для асинхронных запросов




const books = [{
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.getElementById("root");
const ul = document.createElement("ul");
root.append(ul)

const arr = ["author", "name", "price"];

books.forEach(el => {
    const li = document.createElement("li")

    try {
        for (arrItem of arr) {
            if (!el[arrItem]) {
                throw (
                    "No" +
                    arrItem
                );
            } else {
                li.innerText += el[arrItem] + ". ";
            }
        }
        // не работала потому что я прописывала для каждого
            ul.append(li)
        

    } catch (err) {
        console.log("Cannot find " + err)
    }
})



// const books = [{
//     author: "Люсі Фолі",
//     name: "Список запрошених",
//     price: 70
//   },
//   {
//     author: "Сюзанна Кларк",
//     name: "Джонатан Стрейндж і м-р Норрелл",
//   },
//   {
//     name: "Дизайн. Книга для недизайнерів.",
//     price: 70
//   },
//   {
//     author: "Алан Мур",
//     name: "Неономікон",
//     price: 70
//   },
//   {
//     author: "Террі Пратчетт",
//     name: "Рухомі картинки",
//     price: 40
//   },
//   {
//     author: "Анґус Гайленд",
//     name: "Коти в мистецтві",
//   }
// ];


// function makeList(books) {

//   const root = document.getElementById("root");
//   const ul = document.createElement("ul");
//   root.append(ul);


//   books.forEach(function (elem) {

//     try {
//       if (!elem.author || !elem.name || !elem.price) {
//         if (!elem.author) throw "author"
//         if (!elem.name) throw "name"
//         if (!elem.price) throw "price"


//       } else {
//         const list = document.createElement("li");
//         list.append(document.createTextNode(`${elem.author}`));
//         list.append(document.createTextNode(`${elem.name}`));
//         list.append(document.createTextNode(`${elem.price}`));
//         ul.append(list);
//       }
//     } catch(err) {
//       console.log("Cannot find " + err)
//     }

//   })
// }

// makeList(books);

